import Modal from './Modal.js';
import modalInstance from '../script.js';

//Класс для создания карточки
class Card extends Modal {
  constructor(formData, selectedDoctor, receivedId, confirmCallback, title) {
    super(confirmCallback);
    this.title = title;
    this.patientName = formData['patient-name'] || '';
    this.aimVisit = formData['aim-visit'] || '';
    this.descriptionVisit = formData['description-visit'] || '';
    this.urgency = formData['urgency'] || '';
    this.lastVisitDate = formData['last-visit-date'] || '';
    this.bloodPressure = formData['blood-pressure'] || '';
    this.age = formData['age'] || '';
    this.cardioDeseases = formData['cardio-deseases'] || '';
    this.bmi = formData['bmi'] || '';
    this.selectedDoctor = selectedDoctor || '';
    this.receivedId = receivedId || '';
    this.cards = [];

    this.patientNameInput = document.createElement('input');
    this.patientNameInput.type = 'text';
  }

//метод, который ожидает заполнения всех полей формы
  async waitForFormFields(form) {
    return new Promise((resolve) => {
      const interval = setInterval(() => {
        const patientNameInput = form.querySelector('#patient-name');
        const aimVisitInput = form.querySelector('#aim-visit');
        const descriptionVisitInput = form.querySelector('#description-visit');
        const urgencyInput = form.querySelector('#urgency');
        const lastVisitDateInput = form.querySelector('#last-visit-date');
        const bloodPressureInput = form.querySelector('#blood-pressure');
        const ageInput = form.querySelector('#age');
        const cardioDeseasesInput = form.querySelector('#cardio-deseases');
        const bmiInput = form.querySelector('#bmi');

        if (
          patientNameInput &&
          aimVisitInput &&
          descriptionVisitInput &&
          urgencyInput &&
          lastVisitDateInput &&
          bloodPressureInput &&
          ageInput &&
          cardioDeseasesInput &&
          bmiInput &&
          patientNameInput.value &&
          aimVisitInput.value &&
          descriptionVisitInput.value &&
          urgencyInput.value &&
          lastVisitDateInput.value &&
          bloodPressureInput.value &&
          ageInput.value &&
          cardioDeseasesInput.value &&
          bmiInput.value
        ) {
          clearInterval(interval);
          this.patientName = patientNameInput.value;
          this.aimVisit = aimVisitInput.value;
          this.descriptionVisit = descriptionVisitInput.value;
          this.urgency = urgencyInput.value;
          this.lastVisitDate = lastVisitDateInput.value;
          this.bloodPressure = bloodPressureInput.value;
          this.age = ageInput.value;
          this.cardioDeseases = cardioDeseasesInput.value;
          this.bmi = bmiInput.value;
          resolve();
        }
      }, 100);
    });
  }
//метод для удаления карточки
  async deleteCard() {
    try {
      if (!this.receivedId) {
        console.log("delCarFirst: ", this.receivedId);
        console.error('Помилка при видаленні картки: Не вказано receivedId');
        return;
      }
      await this.deleteCardFromServer(this.receivedId);
    } catch (error) {
      console.error('Помилка при видаленні картки:', error);
    }
  }

//метод, работающий с сервером для получения удаления карточки с сервера
  async deleteCardFromServer(receivedId) {
    try {
      const response = await axios.delete(`https://ajax.test-danit.com/api/v2/cards/${this.receivedId}`, {
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });

      if (response.status === 200) {
        console.log('Картка успішно видалена');
        console.log("deleteCardFromReceivedID: ", this.receivedId);
      } else {
        console.error('Помилка при видаленні картки: Неправильний статус відповіді від сервера.');
      }
    } catch (error) {
      console.error('Помилка при видаленні картки:', error);
    }
  }

//метод, который рендерит карточку
  renderCard(cardContainer) {

    const cardData = {
      patientName: this.patientName,
      aimVisit: this.aimVisit,
      descriptionVisit: this.descriptionVisit,
      urgency: this.urgency,
      lastVisitDate: this.lastVisitDate,
      bloodPressure: this.bloodPressure,
      age: this.age,
      cardioDeseases: this.cardioDeseases,
      bmi: this.bmi,
      selectedDoctor: this.selectedDoctor,
      receivedId: this.receivedId,
    };

    let searchInput = document.getElementById("search");
    if (cardData.patientName.toLowerCase().includes(searchInput.value.toLowerCase().trim())) {
      const cardTemplate = document.querySelector('#card-template').content.cloneNode(true);
      this.card = cardTemplate.querySelector('.card');
      this.card.querySelector('.patient-name').textContent = `ПІБ пацієнта: ${this.patientName}`;
      this.card.querySelector('.doctor-name').textContent = `Запис створено до лікаря: ${this.selectedDoctor}`;

      this.closeBtn = this.card.querySelector('.close-btn');
      //проверка на удаление и удаление карточки
      this.closeBtn.onclick = async (event) => {
        event.stopPropagation(); 

        const confirmed = window.confirm('Ви впевнені, що бажаєте видалити картку?');

        if (confirmed) {
          await this.deleteCard(this.receivedId);
          this.cardContainer.removeChild(this.card);
        } else {
          console.log('Відміна видалення картки');
        }
      };

      this.cardContainer = document.querySelector('.cardContainer');
      this.cardContainer.appendChild(this.card);
      const editButton = document.createElement('button');
      editButton.textContent = 'Редагувати';
      editButton.classList.add('btn', 'btn-warning', 'js-modal-warning-btn', 'createBTN');

      //код, отвечающий за нажатие по карточке для открытия информации о ней
      this.card.onclick = async () => {
        try {
          const modalFormFields = new Modal(() => { });
          modalFormFields.configure();
          modalInstance.show();

          const cardData = await this.getCardDetails(this.receivedId);

          modalFormFields.body.innerHTML = '';

          const existingEditButton = modalFormFields.modal.querySelector('.btn-warning');
          const modalTitle = document.createElement('h3');
          const modalContent = document.createElement('form');
          modalContent.id = 'editForm';
          modalContent.classList.add('cardValue');
          let title = document.querySelector(".modal-title");

          const modalEditsButtons = modalFormFields.modal.querySelectorAll('.btn-warning')

          modalEditsButtons.forEach((item) => {
            item.remove()
          }

          )

          this.editButton = document.createElement('button');
          this.editButton.classList.add('btn', 'btn-warning');
          this.editButton.textContent = 'Редагувати';
          modalFormFields.modal.querySelector('.modal-footer').prepend(editButton);
          this.editButton = editButton;

          if (title && title.textContent !== 'Інформація про прийом') {
            title.textContent = 'Інформація про прийом';
          }

          //парсинг информации из сервера для каждой карточки + игнорирование АЙДИ
          for (let key in cardData) {
            if (key !== 'id') {

              switch (key) {
                case 'patient-name':
                  const modalTitle = document.createElement('h3');
                  const italicTextName = document.createElement('i');
                  italicTextName.textContent = cardData[key];
                  modalTitle.textContent = 'ПІБ клієнта: ';
                  modalTitle.appendChild(italicTextName);
                  modalContent.prepend(modalTitle);
                  break;

                case 'aim-visit':
                  const aimVisitLabel = document.createElement('label');
                  const italicTextAimVisit = document.createElement('i');
                  italicTextAimVisit.textContent = cardData[key];
                  aimVisitLabel.textContent = 'Ціль візиту: ';
                  aimVisitLabel.appendChild(italicTextAimVisit);
                  modalContent.appendChild(aimVisitLabel);
                  break;

                case 'description-visit':
                  const descriptionLabel = document.createElement('label');
                  const italicTextDescription = document.createElement('i');
                  italicTextDescription.textContent = cardData[key];
                  descriptionLabel.textContent = 'Опис візиту: ';
                  descriptionLabel.appendChild(italicTextDescription);
                  modalContent.appendChild(descriptionLabel);
                  break;

                case 'urgency':
                  const urgencyLabel = document.createElement('label');
                  const italicTextUrgency = document.createElement('i');
                  italicTextUrgency.textContent = cardData[key];
                  urgencyLabel.textContent = 'Терміновість: ';
                  urgencyLabel.appendChild(italicTextUrgency);
                  modalContent.appendChild(urgencyLabel);
                  break;

                case 'statusCard':
                  const statusLabel = document.createElement('label');
                  const italicTextStatus = document.createElement('i');
                  italicTextStatus.textContent = cardData[key];
                  statusLabel.textContent = 'Статус: ';
                  statusLabel.appendChild(italicTextStatus);
                  modalContent.appendChild(statusLabel);
                  break;

                case 'patient-age':
                  const patientAgeLabel = document.createElement('label');
                  const italicTextPatientAge = document.createElement('i');
                  italicTextPatientAge.textContent = cardData[key];
                  patientAgeLabel.textContent = 'Вік пацієнта: ';
                  patientAgeLabel.appendChild(italicTextPatientAge);
                  modalContent.appendChild(patientAgeLabel);
                  break;

                case 'selectedDoctor':
                  const italicText = document.createElement('i');
                  const selectedDoctorLabel = document.createElement('label');
                  italicText.textContent = cardData[key];
                  selectedDoctorLabel.textContent = 'Лікар: ';
                  selectedDoctorLabel.appendChild(italicText);
                  modalContent.appendChild(selectedDoctorLabel);
                  break;

                case 'cardio-deseases':
                  const cardioDeseasesLabel = document.createElement('label');
                  const cardioDeseasesText = document.createElement('span');
                  cardioDeseasesText.textContent = `Серцеві захворювання: `;
                  const cardioDeseasesItalic = document.createElement('i');
                  cardioDeseasesItalic.textContent = cardData[key];
                  cardioDeseasesText.appendChild(cardioDeseasesItalic);
                  cardioDeseasesLabel.appendChild(cardioDeseasesText);
                  modalContent.appendChild(cardioDeseasesLabel);
                  break;

                case 'blood-pressure':
                  const bloodPressureLabel = document.createElement('label');
                  const italicTextPressure = document.createElement('i');
                  italicTextPressure.textContent = cardData[key];
                  bloodPressureLabel.textContent = "Кров'яний тиск: ";
                  bloodPressureLabel.appendChild(italicTextPressure);
                  modalContent.appendChild(bloodPressureLabel);
                  break;

                case 'last-visit-date':
                  const lastDateLabel = document.createElement('label');
                  const italicTextDate = document.createElement('i');
                  italicTextDate.textContent = cardData[key];
                  lastDateLabel.textContent = 'Дата останнього візиту: ';
                  lastDateLabel.appendChild(italicTextDate);
                  modalContent.appendChild(lastDateLabel);
                  break;

                case 'bmi':
                  const bmiAgeLabel = document.createElement('label');
                  const italicTextBMI = document.createElement('i');
                  italicTextBMI.textContent = cardData[key];
                  bmiAgeLabel.textContent = 'Індекс маси тіла: ';
                  bmiAgeLabel.appendChild(italicTextBMI);
                  modalContent.appendChild(bmiAgeLabel);
                  break;

                case 'age':
                  const ageLabel = document.createElement('label');
                  const italicTextAge = document.createElement('i');
                  italicTextAge.textContent = cardData[key];
                  ageLabel.textContent = 'Вік пацієнта: ';
                  ageLabel.appendChild(italicTextAge);
                  modalContent.appendChild(ageLabel);
                  break;

              }
            }
          }

          modalFormFields.body.appendChild(modalContent);

          this.button.textContent = "Save";

          //код отвечает за вызов модального окна "редактировать"
          this.editButton.onclick = () => {

            modalContent.innerHTML = '';

            for (const key in cardData) {
              if (key !== 'id') {

                const inputElement = document.createElement('input');
                inputElement.type = 'text';
                inputElement.name = key;

                inputElement.value = cardData[key];

                const labelElement = document.createElement('label');
                labelElement.textContent = `${key}: `;

                modalContent.appendChild(labelElement);
                modalContent.appendChild(inputElement);
                modalContent.appendChild(document.createElement('br')); 
              }
            }
          };

          const inputElements = {
            patientName: document.querySelector('#patient-name'),
            aimVisit: document.querySelector('#aim-visit'),
            descriptionVisit: document.querySelector('#description-visit'),
            urgency: document.querySelector('#urgency'),
            lastVisitDate: document.querySelector('#last-visit-date'),
            bloodPressure: document.querySelector('#blood-pressure'),
            age: document.querySelector('#age'),
            cardioDeseases: document.querySelector('#cardio-deseases'),
            bmi: document.querySelector('#bmi')
          };

          //код, при открытии "редактировать" выполнит сохранение на сервер измененных данных
          this.button.onclick = () => {
            let updatedData = {}; 
            const formEdit = document.getElementById('editForm');
            let bool = false;

            const formData = new FormData(formEdit);
            for (const [key, value] of formData.entries()) {
              bool = true;
              updatedData[key] = value;
            }
            if (!bool) {
              updatedData = cardData
            };


            axios.put(`https://ajax.test-danit.com/api/v2/cards/${this.receivedId}`, updatedData, {
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
              }
            })
              .then(response => {
                if (response.status === 200) {
                  console.log('Дані успішно оновлені:', response.data);
                  modalInstance.hide();
                } else {
                  throw new Error('Ошибка при обновлении данных о карте');
                }
              })
              .catch(error => {
                console.error('Ошибка при обновлении данных:', error);
              });
          };
        } catch {
          console.error('Ошибка при обновлении данных:');
        }
      }
    }
  }

//метод для получения конкретной карточки из сервера
  async getCardDetails(receivedId) {
    try {
      const response = await axios.get(`https://ajax.test-danit.com/api/v2/cards/${receivedId}`, {
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });

      if (response.status === 200) {
        return response.data;
      } else {
        throw new Error('Помилка при отриманні даних про картку');
      }
    } catch (error) {
      throw new Error('Помилка при отриманні даних про картку:', error);
    }
  }

//метод, который рендерит карточки при использовании фильтров
  static async fetchCardsAndRender(cardContainer, cardData, status, priority, searchText ) {
    try {
      const response = await axios.get('https://ajax.test-danit.com/api/v2/cards', {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });
  
      this.cards = response.data || [];
      let filteredCards = this.cards;
  
      if (status === 'done' || status === 'open') {
        filteredCards = filteredCards.filter(card => card.statusCard === status);
      }
  
      if (priority === 'high' || priority === 'normal' || priority === 'low') {
        filteredCards = filteredCards.filter(card => card.urgency === priority);
      }
  
      cardContainer.innerHTML = '';
      if (filteredCards.length === 0) {
        cardContainer.textContent = 'Карток не знайдено';
      } else {
        filteredCards.forEach(cardData => {
          let { selectedDoctor, receivedId, ...formData } = cardData;
          receivedId = cardData.id;
          const cardInstance = new Card(formData, selectedDoctor, receivedId);
          cardInstance.renderCard(cardContainer);
        });
      }
  
      return filteredCards;
    } catch (error) {
      console.error('Помилка при завантаженні карток:', error);
      return [];
    }
  }
}
// две функции для отфильтровки карточек за указанными параметрами: статус/приоритет 
// + функция рендера отфильтрованых данных
function filterByStatus(cards, status) {
  console.log(cards.filter(card => card.statusCard === status));
}


function filterByPriority(cards, priority) {
  console.log(cards.filter(card => card.urgency === priority));
}

async function displayFilteredResults(status, priority, container) {
  container.innerHTML = '';

  const filteredCards = await Card.fetchCardsAndRender(container, [], status, priority);

  if (filteredCards.length === 0) {
    container.textContent = 'Картки не знайдено.';
  }

  if(!localStorage.getItem('token')){
    container.textContent = 'У вас немає доступу до цих функцій, будь ласка, авторизуйтесь!';
    container.style.color = "red";
    container.style.backgroundColor = "white";
    container.style.textAlign = "center";
    container.style.width = "100%";
    container.style.display = "grid";
    container.style.gridTemplateColumns = "1fr"; 
  }
}

const statusOpen = document.getElementById('statusOpen');
const statusDone = document.getElementById('statusDone');
const cardContainer = document.querySelector('.cardContainer');

//листенеры для работы фильтрации
statusOpen.addEventListener('change', async () => {
  if (statusOpen.checked) {
    const priorityFilter = document.querySelector('input[name="priority"]:checked');
    const priorityValue = priorityFilter ? priorityFilter.value : '';

    await displayFilteredResults('open', priorityValue, cardContainer);
  }
});

statusDone.addEventListener('change', async () => {
  if (statusDone.checked) {
    const priorityFilter = document.querySelector('input[name="priority"]:checked');
    const priorityValue = priorityFilter ? priorityFilter.value : '';

    await displayFilteredResults('done', priorityValue, cardContainer);
  }
});

document.querySelectorAll('input[name="priority"]').forEach(radio => {
  radio.addEventListener('change', async event => {
    const statusFilter = document.querySelector('input[name="status"]:checked');
    const statusValue = statusFilter ? statusFilter.value : '';

    const priorityValue = event.target.value;

    await displayFilteredResults(statusValue, priorityValue, cardContainer);
  });
});

export default Card;

